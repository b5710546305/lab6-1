import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * This class counts how many times different words occur.
 * Each time the addWord() method is called, you add to the occurrence for that word.
 * Ignore the case of words in counting words.
 * @author Parinvut Rochanavedya
 * @version 24-02-2015
 */
public class WordCounter {
	
	/**Set of words seen*/
	private Map<String,Integer> words;

	/**
	 * Constructor of word counter class
	 */
	public WordCounter(){
		words = new HashMap<String,Integer>();
	}
	
	/**
	 * Add one to the count of occurrences for this word, ignoring case.
	 * @param word to add
	 */
	public void addWord(String word){
		if (words.containsKey(word)){
			words.put(word, words.get(word)+1);
		} else {
			words.put(word, 1);
		}
	}
	
	/**
	 * Get all the words seen so far, as a Set
	 * @return set of words
	 */
	public Set<String> getWords(){
		return words.keySet();
	}
	
	/**
	 * Get the number of occurrences for a given word.
	 * @param given word
	 * @return wordCount
	 */
	public int getCount(String word){
		return words.get(word);
	}
	
	/**
	 * Get all the words seen so far, sorted in alphabetical order.
	 * @return String array of sorted words
	 */
	public String[] getSortedWords(){
		ArrayList<String> arrList = new ArrayList<String>(getWords());
		Collections.sort(arrList);
		String[] str = new String[arrList.size()];
		arrList.toArray(str);
		return str;
	}
	
}
