import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Word count class that uses WordCounter class
 * to read txt file and count words in it.
 * @author Parinvut Rochanavedya
 * @version 24-02-2015
 */
public class WordCount {

	/**
	 * Application runner
	 * @param args do nothing
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		final String FILE_URL = "https://bitbucket.org/skeoop/oop/raw/master/week6/Alice-in-Wonderland.txt";
		URL url = new URL(FILE_URL);
		InputStream input = url.openStream();
		Scanner scanner = new Scanner(input);
		
		final String DELIMS = "[\\s,.\\?!\"():;]+";
		scanner.useDelimiter(DELIMS);
		
		WordCounter counter = new WordCounter();
		
		//Print the first 20 words : number of word occurence
			counter.addWord(scanner.next());
			counter.addWord(scanner.next());
			counter.addWord(scanner.next());
			counter.addWord(scanner.next());
			counter.addWord(scanner.next());
			counter.addWord(scanner.next());
			counter.addWord(scanner.next());
			counter.addWord(scanner.next());
			counter.addWord(scanner.next());
			counter.addWord(scanner.next());
			counter.addWord(scanner.next());
			counter.addWord(scanner.next());
			counter.addWord(scanner.next());
			counter.addWord(scanner.next());
			counter.addWord(scanner.next());
			counter.addWord(scanner.next());
			counter.addWord(scanner.next());
			counter.addWord(scanner.next());
			counter.addWord(scanner.next());
			counter.addWord(scanner.next());
			counter.addWord(scanner.next());
			counter.addWord(scanner.next());
		
		for(int i = 0; i < counter.getWords().size(); i++){
			System.out.println(counter.getWords().toArray()[i] + " : " + counter.getCount((String)counter.getWords().toArray()[i]));
		}
		
		String[] fq = counter.getSortedWords();
		for(int i = 0; i < fq.length; i++){
			System.out.print(fq[i] + " ");
		}
		
		
		
		
		
	}

}
